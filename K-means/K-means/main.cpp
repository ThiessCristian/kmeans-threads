﻿#include "Coord.h"
#include "Rgb.h"
#include "Img.h"
#include <unordered_map>
#include <map>
#include <iostream>
#include <fstream>

int main(/*const int argc, const char* const* const argv*/)
{
   /*
	if (argc < 3)
	{
		std::cerr << "2 arguments are required: file and number of clusters\n";
		exit(EXIT_FAILURE);
	}

	const char* const file = argv[1];*/
//	const auto nbOfClusters = atoi(argv[2]);

   //const char* file = "C:\\Users\\Cristi\\Desktop\\in.txt";
   //const char* file = "C:\\Users\\Cristi\\Desktop\\image.txt";
   const char* file = "in2.txt";
	auto initialClusterGroups = std::vector<Rgb>() =
	{
		{111 , 87 , 151},
		{158 , 230 , 230},
		{158 , 91 , 212},
	};

	auto img = Img(file);
	const auto result = img.makeAllClusters(initialClusterGroups);
	
   std::ofstream out("out1.txt");
   out << img.getWidth() << ", " << img.getHeight()<<std::endl;
	printf("%d, %d\n", img.getWidth(), img.getHeight());
	for (const auto& cluster : result)
	{
		for (const auto& coord : cluster.coords)
		{
			const auto& color = cluster.center;
         /*
			printf("[ %d,%d ] = [ %d,%d,%d ]\n",
				coord.x, coord.y,
				color.r, color.g, color.b);
            */
         out << "[ " << coord.x << "," << coord.y << " ] = [ ";
         out << color.r << "," << color.g << "," << color.b <<" ]"<< std::endl;
		}
	}
	
	return 0;
}
