#include "Img.h"
#include "Cluster.h"
#include <fstream>
#include <iostream>
#include <cmath>
#include <string>
#include <algorithm>
#include <thread>
#include <atomic>

Img::Img(const char* const inputFile)
{
	std::ifstream fileStream;

	fileStream.open(inputFile);
	if (!fileStream.is_open())
	{
		std::cerr << inputFile << ": " << strerror(errno) << std::endl;
		exit(EXIT_FAILURE);
	}
	
	std::string buf;
	std::getline(fileStream, buf);
	std::sscanf(buf.c_str(), "%d , %d", &m_width, &m_height);
	
	for (int i = 0; i < m_width * m_height; i++)
	{
		auto coord = Coord();
		auto rgb = Rgb();

		std::getline(fileStream, buf);
		std::sscanf(
			buf.c_str(),
			"[ %d , %d ] = [ %d , %d , %d ]",
			&coord.x, &coord.y,
			&rgb.r, &rgb.g, &rgb.b);
		
		m_colorMap.insert(std::make_pair(coord, rgb));
	}
}

/*
** Public methods.
*/

int Img::getWidth() const
{
	return m_width;
}
int Img::getHeight() const
{
	return m_height;
}

const std::unordered_map<Coord, Rgb>& Img::getColorMap() const
{
	return m_colorMap;
}

std::vector<Cluster> Img::makeAllClusters(std::vector<Rgb> clusterGroups) const
{
   std::cout << "done: 0"<< std::endl;
	auto clusters = computeClusters(clusterGroups);
	
	for (int i = 0; i < 6; i++)
	{
		std::transform(clusters.begin(), clusters.end(), clusterGroups.begin(),
			[](const auto& item)
			{
				return item.center;
			});
      std::cout << "done: " << i+1 << std::endl;
		clusters = computeClusters(clusterGroups);

	}
	
	return clusters;
}

/*
** Private methods.
*/

double Img::computeColorDistance(const Rgb& first, const Rgb& second)
{
   int r = first.r - second.r;
   int g = first.g - second.g;
   int b = first.b - second.b;

	return std::sqrt(r*r+g*g+b*b);
}

Rgb Img::getCenter(const std::vector<Coord>& coords) const
{
	Rgb result(0, 0, 0);
	
	for (const auto& coord : coords)
	{
		const auto& color = m_colorMap.at(coord);
		result.r += color.r;
		result.g += color.g;
		result.b += color.b;
	}
	
	result.r /= coords.size();
	result.g /= coords.size();
	result.b /= coords.size();
	return result;
}

static Cluster& getClosestCluster(std::vector<Cluster>& clusters,const Rgb& color)
{
	Cluster* minCluster = nullptr;
	double minDist = std::numeric_limits<double>::max();
	
	for (auto& cluster : clusters)
	{
		const double dist = Img::computeColorDistance(cluster.center, color);
		if (dist < minDist)
		{
			minDist = dist;
			minCluster = &cluster;
		}
	}
	
	return *minCluster;
}

void Img::computeSinglePoint(const std::pair<const Coord,Rgb>& point, std::vector<Cluster>& clusters) const
{
   const auto& coord = point.first;
   const auto& color = point.second;

   auto& closestCluster = getClosestCluster(clusters, color);
   //std::lock_guard<std::mutex> lockGuard(mutex);
   mutex.lock();
   closestCluster.coords.push_back(coord);
   mutex.unlock();
}

void Img::computeAllPointsNoThreads(std::vector<Cluster>& clusters) const
{
   for (const auto& coordAndRgb : m_colorMap) {
      computeSinglePoint(coordAndRgb, clusters);
   }
}


void Img::computeAllPointsThreads(std::vector<Cluster>& clusters,const size_t& nrOfThreads) const
{
   m_nrOfThreads = nrOfThreads;

   std::vector<std::thread> threads;
   threads.resize(m_nrOfThreads);
   size_t count = 0;
   size_t nrOfPointsToBeComputed = (m_width*m_height) / m_nrOfThreads;
   for (auto it = m_colorMap.begin(); it != m_colorMap.end();) {

      //save the beginning 
      auto temp = it;

      //at the last iteration jump right to end
      if (count == m_nrOfThreads - 1) {
         it = m_colorMap.end();
      } else {
         std::advance(it, nrOfPointsToBeComputed);
      }

      auto computeGroupsPoints = [this, &clusters](const auto& begin, const auto& end) {
         for (auto it2 = begin; it2 != end; ++it2) {
            computeSinglePoint(*it2, clusters);
         }
      };

      threads[count++] = std::thread(computeGroupsPoints, temp, it);
   }
   for (auto& thread : threads) {
      if (thread.joinable()) {
         thread.join();
      }
   }
}

std::vector<Cluster> Img::computeClusters(std::vector<Rgb> clusterGroups) const
{
	std::vector<Cluster> clusters;
	
	for (const auto& clusterGroup : clusterGroups)
	{
		auto cluster = Cluster();
		cluster.center = clusterGroup;
		
		clusters.push_back(cluster);
	}
   auto begin = std::chrono::high_resolution_clock::now();
   

   //computeAllPointsNoThreads(clusters);


   computeAllPointsThreads(clusters, 30);

   auto end = std::chrono::high_resolution_clock::now();
   std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count()<<" milliseconds "<<std::endl;

   for (auto& cluster : clusters) {
      cluster.center = getCenter(cluster.coords);
   }

	
	return clusters;
}

/*
** Operators.
*/

std::ostream& operator << (std::ostream& o, const Img& target)
{
	for (const auto& pair : target.m_colorMap)
	{
		std::cout << pair.first << ": " << pair.second << std::endl;
	}
	return o;
}
